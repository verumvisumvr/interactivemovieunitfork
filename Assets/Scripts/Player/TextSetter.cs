﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextSetter : MonoBehaviour {

	[SerializeField]
	Text targetText;
	[SerializeField]
	string[] linesToSet;

	public void SetText() {
		//targetText.text = textToSet;
		targetText.text = "";
		foreach (string s in linesToSet) {
			targetText.text += s + "\n";
		}
	}
}

