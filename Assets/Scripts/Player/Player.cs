﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	[SerializeField]
	Texture initialImage;
	[SerializeField]
	MediaPlayerCtrl mpc;
	[SerializeField]
	string videoName = "";

	public void PlayVideo() {
		mpc.Load (videoName);
	}

	public void StopVideo() {
		mpc.Stop ();
		mpc.UnLoad ();
		foreach (GameObject go in mpc.m_TargetMaterial) {
			go.GetComponent<Renderer> ().material.mainTexture = initialImage;
		}
	}

	public void PauseVideo() {
		mpc.Pause ();
	}

	public void UnPauseVideo() {
		if(mpc.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED) {
			mpc.Play ();
		}
	}
}
