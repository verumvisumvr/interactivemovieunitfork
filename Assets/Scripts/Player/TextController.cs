﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour {

	[SerializeField]
	Animator animator;
	[SerializeField]
	string fadeTriggerName = "";
	[SerializeField]
	string unFadeTriggerName = "";

	public void Fade() {
		animator.SetTrigger (fadeTriggerName);
	}

	public void UnFade() {
		animator.SetTrigger (unFadeTriggerName);
	}

}
