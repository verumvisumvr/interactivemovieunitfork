﻿using UnityEngine;
using System.Collections;

public class buttonPusher : MonoBehaviour {

	[SerializeField]
	private float RayLength;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		RaycastHit hit;
		if (Physics.Raycast (transform.position, transform.forward, out hit, RayLength)) {

			if (hit.transform.gameObject.GetComponent<oButton> ()) {
				hit.transform.gameObject.GetComponent<oButton> ().PressButton ();
				hit.transform.gameObject.GetComponent<oButton> ().HoldGaze ();
			}
		}
	}
}
