﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonVisualChanger : MonoBehaviour {

	//[SerializeField]
	public bool AnimatedButton;

	private SpriteRenderer spr;
	private Image img;
	private Material mat;
	private Animator anim;

	//[SerializeField]
	[HideInInspector]
	public Color PressedColor, PressColor, UnpressedColor;
	//[SerializeField]
	[HideInInspector]
	public string PressedAnimationName, PressAnimationName, UnpressedAnimationName;

	void Start () {
		//if (SpriteButton)
						spr = gameObject.GetComponent<SpriteRenderer> ();
		//if (UIButton)
						img = gameObject.GetComponent<Image> ();
		//if (AnimatedButton)
						anim = gameObject.GetComponent<Animator> ();
		//if (MeshButton)
			mat = gameObject.GetComponent<Renderer> ().material;

	}
	
	// Update is called once per frame
	void Update () {

	}

	public void ButtonUnPressed(){
		if (spr)
			spr.color = UnpressedColor;
		if (img)
			img.color = UnpressedColor;
		if (mat)
			mat.color = UnpressedColor;
		if (AnimatedButton) {
			anim.SetBool (UnpressedAnimationName,true);
			anim.SetBool (PressedAnimationName,false);
			anim.SetBool (PressAnimationName,false);				
		}
	}

	public void ButtonPressed(){
		if (spr)
			spr.color = PressColor;
		if (img)
			img.color = PressColor;
		if (mat)
			mat.color = PressColor;
		if (AnimatedButton) {
			anim.SetBool (UnpressedAnimationName,false);
			anim.SetBool (PressedAnimationName,false);
			anim.SetBool (PressAnimationName,true);				
		}
	}

	public void ButtonActivated(){
		if (spr)
			spr.color = PressedColor;
		if (img)
			img.color = PressedColor;
		if (mat)
			mat.color = PressedColor;
		if (AnimatedButton) {
			anim.SetBool (UnpressedAnimationName,false);
			anim.SetBool (PressedAnimationName,true);
			anim.SetBool (PressAnimationName,false);				
		}
	}

}
