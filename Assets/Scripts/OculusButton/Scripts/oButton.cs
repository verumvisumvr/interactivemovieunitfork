﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

[RequireComponent (typeof (ButtonVisualChanger))]
public class oButton : MonoBehaviour {

	public bool Pressed;

	private ButtonVisualChanger BVC;
	private SpriteRenderer buttonImg;

	public class TestEvent : UnityEvent {}


	[SerializeField]
	private bool singlePress;

	public float CurrentPressingTime { private set; get; }
	private float PressingMissTime;
	private float GazeMissTime;
	public float TimeForPress;

	private bool itsPresed;
	private bool gazeHold = false;

	[SerializeField]
	private UnityEvent pressEvent;
	[SerializeField]
	private UnityEvent enterEvent;
	[SerializeField]
	private UnityEvent exitEvent;
	// Use this for initialization
	void Start () {
		BVC = gameObject.GetComponent<ButtonVisualChanger> ();
		buttonImg = gameObject.GetComponent<SpriteRenderer> ();

		Pressed = false;
		CurrentPressingTime = TimeForPress;
	}
	
	// Update is called once per frame
	void Update () {

		if (PressingMissTime > 0.001f) {
			itsPresed=false;
			Pressed=false;
			CurrentPressingTime=TimeForPress;
			BVC.ButtonUnPressed();
		}

		if (PressingMissTime < 2) {
			PressingMissTime += Time.deltaTime;	
		}

		if (GazeMissTime > 0.001f) {
			if (gazeHold) {
				exitEvent.Invoke ();
				Debug.Log ("Invoking exit");
			} 
			gazeHold = false;
		}

		if (GazeMissTime < 1) {
			GazeMissTime += Time.deltaTime;
		}
	}

	public void PressButton(){
		BVC.ButtonPressed();
		PressingMissTime = 0;
		//print (CurrentPressingTime);
		if(CurrentPressingTime>0)
		CurrentPressingTime -= Time.deltaTime;

		if (CurrentPressingTime <= 0 && !itsPresed) {
			BVC.ButtonActivated();
			Pressed = true;
			pressEvent.Invoke();
			if(singlePress)
				SinglePress();
		}

	}

	public void HoldGaze() {
		GazeMissTime = 0f;
		if (!gazeHold) {
			Debug.Log ("Invoking enter");
			enterEvent.Invoke ();
		}
		gazeHold = true;
	}

	public void SinglePress(){
		if (Pressed) {
						Pressed = false;
						itsPresed = true;
						CurrentPressingTime=TimeForPress;
						BVC.ButtonUnPressed();
				}
	}

}
